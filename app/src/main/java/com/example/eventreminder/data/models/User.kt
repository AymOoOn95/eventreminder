package com.example.eventreminder.data.models

class User(
    var googleEmail: String? = null,
    var facebookApiToken: String? = null,
    var facebookEmail: String? = null,
    var isGoogleSignedIn: Boolean? = null,
    var isFacebookSignedIn: Boolean? = null
)