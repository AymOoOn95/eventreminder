package com.example.eventreminder.data.models

class Event(
    val id: Long,
    val cId: Long,
    val organizer: String,
    val title: String,
    val date: Long,
    val endTime: Long,
    val duration: String?,
    val location: String?
) {
    var hasConflict = false

    fun isOverlapped(event: Event):Boolean{
        if (event.date in this.date .. this.endTime || this.date in event.date .. event.endTime){
           hasConflict = true
        }
        return hasConflict
    }
}