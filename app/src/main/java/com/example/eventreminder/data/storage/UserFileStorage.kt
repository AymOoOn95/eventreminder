package com.example.eventreminder.data.storage

import com.example.eventreminder.data.models.User
import io.reactivex.Maybe
import io.reactivex.Observable

interface UserFileStorage {

    fun saveGoogleSignInState(boolean: Boolean)
    fun saveFacebookSignInState(boolean: Boolean)
    fun saveUser(user: User)
    fun userData():Maybe<User>
}