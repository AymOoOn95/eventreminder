package com.example.eventreminder.data.repo

import com.example.eventreminder.data.models.User
import com.example.eventreminder.data.storage.UserFileStorage
import io.reactivex.Maybe

class UserRepo(private val userFileStorage: UserFileStorage) {

    fun saveGoogleSignInState(state: Boolean) {
        this.userFileStorage.saveGoogleSignInState(state)
    }

    fun saveFacebookSignInState(state: Boolean) {
        this.userFileStorage.saveFacebookSignInState(state)
    }

    fun saveUserDataInFile(user: User) {
        this.userFileStorage.saveUser(user)
    }

    fun userData(): Maybe<User> {
        return this.userFileStorage.userData()
    }
}