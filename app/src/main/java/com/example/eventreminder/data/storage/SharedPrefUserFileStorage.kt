package com.example.eventreminder.data.storage

import android.content.SharedPreferences
import androidx.core.content.edit
import com.example.eventreminder.data.models.User
import com.example.eventreminder.util.Constants
import io.reactivex.Maybe

class SharedPrefUserFileStorage(private val sharedPreferences: SharedPreferences) : UserFileStorage {
    override fun saveGoogleSignInState(state: Boolean) {
        sharedPreferences.edit {
            putBoolean(Constants.GOOGLE_LOGIN, state)
        }
    }

    override fun saveFacebookSignInState(state: Boolean) {
        sharedPreferences.edit {
            putBoolean(Constants.FACEBOOK_LOGIN, state)
        }
    }

    override fun saveUser(user: User) {
        sharedPreferences.edit {
            user.googleEmail?.let {
                putString(Constants.GOOGLE_EMAIL, it)
                putBoolean(Constants.GOOGLE_LOGIN, user.isGoogleSignedIn ?: false)
            }
            user.facebookEmail?.let {
                putString(Constants.FACEBOOK_EMAIL, it)
                putString(Constants.FACEBOOK_API_TOKEN, user.facebookApiToken)
                putBoolean(Constants.FACEBOOK_LOGIN, user.isFacebookSignedIn ?: false)
            }

        }
    }

    override fun userData(): Maybe<User> {
        return Maybe.fromCallable<User> {
            var user: User? = null
            if (sharedPreferences.getBoolean(
                    Constants.GOOGLE_LOGIN,
                    false
                ) || sharedPreferences.getBoolean(Constants.FACEBOOK_LOGIN, false)
            ) {
                user = User(
                    sharedPreferences.getString(Constants.GOOGLE_EMAIL, null),
                    sharedPreferences.getString(Constants.FACEBOOK_API_TOKEN, null),
                    sharedPreferences.getString(Constants.FACEBOOK_EMAIL, null),
                    sharedPreferences.getBoolean(Constants.GOOGLE_LOGIN, false),
                    sharedPreferences.getBoolean(Constants.FACEBOOK_LOGIN, false)
                )
            }
            return@fromCallable user
        }
    }
}