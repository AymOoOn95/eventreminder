package com.example.eventreminder.data.repo

import android.content.ContentResolver
import android.provider.CalendarContract
import android.provider.ContactsContract
import com.example.eventreminder.data.models.Event
import com.example.eventreminder.data.models.User
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.facebook.GraphResponse
import com.facebook.HttpMethod
import io.reactivex.Observable
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class EventsRepo(private val contentResolver: ContentResolver) {
    fun GoogleCalendarEvents(organizerEmail: String): Observable<ArrayList<Event>> {
        val cur = contentResolver.query(
            CalendarContract.Events.CONTENT_URI,
            null,
            "${CalendarContract.Events.CALENDAR_ID} != ?",
            arrayOf("1"),
            null
        )
        val events = arrayListOf<Event>()
        while (cur.moveToNext()) {
            var event = Event(
                cur.getLong(cur.getColumnIndex(CalendarContract.Events._ID)),
                cur.getLong(cur.getColumnIndex(CalendarContract.Events.CALENDAR_ID)),
                cur.getString(cur.getColumnIndex(CalendarContract.Events.ORGANIZER)),
                cur.getString(cur.getColumnIndex(CalendarContract.Events.TITLE)),
                cur.getLong(cur.getColumnIndex(CalendarContract.Events.DTSTART)),
                cur.getLong(cur.getColumnIndex(CalendarContract.Events.DTEND)),
                cur.getString(cur.getColumnIndex(CalendarContract.Events.DURATION)),
                cur.getString(cur.getColumnIndex(CalendarContract.Events.EVENT_LOCATION))
            )
            events.add(event)
        }
        cur.close()
        return Observable.fromArray(events)
    }

    fun facebookEvents(user: User) {
        GraphRequest(AccessToken.getCurrentAccessToken(), "/${user.facebookEmail}/events", null, HttpMethod.GET,
            GraphRequest.Callback {
                Timber.d(it.jsonObject.toString(4))
            }).executeAsync()

    }
}