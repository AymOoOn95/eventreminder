package com.example.eventreminder.data.network

import com.example.eventreminder.util.Constants
import retrofit2.http.GET
import retrofit2.http.Query

interface OpenWeatherApi {

    @GET("forecast/hourly")
    fun getWeatherForecast(@Query("lat") lat: Long, @Query("lon") lon: Long, @Query("appid") apiKey: String = Constants.OPEN_WEATHER_API_KEY)
}