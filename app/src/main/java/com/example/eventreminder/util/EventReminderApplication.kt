package com.example.eventreminder.util

import android.app.Application
import com.example.eventreminder.di.*
import timber.log.Timber

class EventReminderApplication : Application() {

    var appComponent: AppComponent? = null
    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        appComponent = DaggerAppComponent.builder().repoModule(RepoModule(contentResolver))
            .storageModule(StorageModule(applicationContext)).networkModule(NetworkModule()).build()

    }
}