package com.example.eventreminder.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.util.Base64
import android.widget.Toast
import timber.log.Timber
import java.lang.Exception
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


fun Context.makeToast(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}

fun Activity.startNextActivity(activityClass: Class<*>) {
    this.startActivity(Intent(this, activityClass))
}


fun Activity.printHashKey() {
    try {
        val info = packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES)
        for (signature in info.signatures) {
            val md = MessageDigest.getInstance("SHA")
            md.update(signature.toByteArray())
            val hashKey = String(Base64.encode(md.digest(), 0))
            Timber.d("printHashKey() Hash Key: $hashKey")
        }
    } catch (e: NoSuchAlgorithmException) {
        Timber.e("printHashKey() $e")
    } catch (e: Exception) {
        Timber.e("printHashKey() $e")
    }

}