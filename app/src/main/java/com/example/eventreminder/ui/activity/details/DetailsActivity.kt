package com.example.eventreminder.ui.activity.details

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.eventreminder.R

class DetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        // todo get weather forecasts and handle location permission
    }
}
