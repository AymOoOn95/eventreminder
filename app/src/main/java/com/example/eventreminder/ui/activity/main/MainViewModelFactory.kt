package com.example.eventreminder.ui.activity.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.eventreminder.data.repo.EventsRepo
import com.example.eventreminder.data.repo.UserRepo

class MainViewModelFactory(private val userRepo: UserRepo, private val eventsRepo: EventsRepo) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainViewModel(userRepo, eventsRepo) as T
    }
}