package com.example.eventreminder.ui.activity.accounts

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.example.eventreminder.R
import com.example.eventreminder.data.models.User
import com.example.eventreminder.data.repo.UserRepo
import com.example.eventreminder.ui.activity.main.MainActivity
import com.example.eventreminder.util.Constants
import com.example.eventreminder.util.EventReminderApplication
import com.example.eventreminder.util.makeToast
import com.example.eventreminder.util.startNextActivity
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import kotlinx.android.synthetic.main.activity_accounts_linking.*
import timber.log.Timber
import java.lang.Exception
import javax.inject.Inject


class AccountsLinkingActivity : AppCompatActivity() {
    private val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
        .requestEmail()
        .requestIdToken(Constants.CLIENT_ID)
        .build()

    private val callbackManager = CallbackManager.Factory.create()

    @Inject
    lateinit var userRepo: UserRepo

    var viewModel: AccountLinkingViewModel? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_accounts_linking)
        (application as EventReminderApplication).appComponent?.inject(this)


        if (this::userRepo.isInitialized) {
            viewModel = ViewModelProviders.of(this, AccountLinkingViewModelFactory(userRepo))
                .get(AccountLinkingViewModel::class.java)
        }


        checkAccounts()

        nextBtn.setOnClickListener {
            this.startNextActivity(MainActivity::class.java)
        }
        googleBtn.setOnClickListener {
            authenticateGoogleAccount()
        }

        facebookBtn.setPermissions(mutableListOf("email", "user_events"))
        facebookBtn.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                viewModel?.let {
                    it.saveUserData(
                        User(
                            isFacebookSignedIn = true,
                            facebookApiToken = result?.accessToken?.token,
                            facebookEmail = result?.accessToken?.userId
                        )
                    )
                    nextBtn.visibility = View.VISIBLE
                } ?: this@AccountsLinkingActivity.makeToast("An error occurred while saving data")
                this@AccountsLinkingActivity.makeToast("Done success")
            }

            override fun onCancel() {
                this@AccountsLinkingActivity.makeToast("cancel ")
            }

            override fun onError(error: FacebookException?) {
                this@AccountsLinkingActivity.makeToast("An error while signing in")
                Timber.e(error)

            }

        })

    }

    private fun authenticateGoogleAccount() {
        val account = GoogleSignIn.getLastSignedInAccount(this)
        account?.email?.let { email ->
            viewModel?.let {
                it.saveUserData(User(googleEmail = email, isGoogleSignedIn = true))
                nextBtn.visibility = View.VISIBLE
            } ?: this.makeToast("An error occurred while saving data")
            Timber.d("email from last sign in is $email")
        } ?: run {
            val signInClient = GoogleSignIn.getClient(this, gso)
            startActivityForResult(signInClient.signInIntent, Constants.GOOGLE_SIGN_IN_REQUEST_CODE)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.GOOGLE_SIGN_IN_REQUEST_CODE) {
            try {
                GoogleSignIn.getSignedInAccountFromIntent(data).result?.let { account ->
                    account.email?.let { email ->
                        Timber.d("email from new sign in is $email")
                        viewModel?.let {
                            it.saveUserData(User(googleEmail = email, isGoogleSignedIn = true))
                            nextBtn.visibility = View.VISIBLE
                        } ?: this.makeToast("An error occurred while saving data")
                    }
                }
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    private fun checkAccounts() {
        val googleEmail = GoogleSignIn.getLastSignedInAccount(this)?.email

        val accessToken = AccessToken.getCurrentAccessToken()
        val isLoggedIn = accessToken != null && !accessToken.isExpired

        if (googleEmail != null) {
            googleBtn.visibility = View.GONE
            Timber.d("${AccessToken.getCurrentAccessToken()}")

            this.startNextActivity(MainActivity::class.java)
            finish()

        }

    }


}
