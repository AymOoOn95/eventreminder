package com.example.eventreminder.ui.activity.main

import android.Manifest
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.eventreminder.R
import com.example.eventreminder.data.models.Event
import com.example.eventreminder.data.repo.EventsRepo
import com.example.eventreminder.data.repo.UserRepo
import com.example.eventreminder.ui.adapter.EventsAdapter
import com.example.eventreminder.util.Constants
import com.example.eventreminder.util.EventReminderApplication
import com.example.eventreminder.util.makeToast
import com.facebook.AccessToken
import kotlinx.android.synthetic.main.activity_main.*
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import javax.inject.Inject
import com.facebook.CallbackManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import ru.slybeaver.slycalendarview.SlyCalendarDialog
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity() {
    @Inject
    lateinit var eventsRepo: EventsRepo

    @Inject
    lateinit var userRepo: UserRepo

    private val viewModel: MainViewModel by lazy {
        ViewModelProviders.of(this, MainViewModelFactory(userRepo, eventsRepo)).get(MainViewModel::class.java)
    }

    private val compositeDisposable = CompositeDisposable()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        (application as EventReminderApplication).appComponent?.inject(this)


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR)
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR),
                Constants.READ_WRITE_CALENDER_REQUEST_CODE
            )

        } else {
            loadEvents()
        }


    }


    private fun loadEvents() {
        compositeDisposable.add(
            viewModel.userData().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
                {
                    viewModel.loadEvents(it)?.let {
                        it.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({
                            eventRecycler.adapter = EventsAdapter(it, object : EventsAdapter.OnPostponeClickListner {
                                override fun onPostponeClick(event: Event) {
                                    SlyCalendarDialog()
                                        .setSingle(true)
                                        .setHeaderColor(getColor(R.color.colorPrimary))
                                        .setCallback(object : SlyCalendarDialog.Callback {
                                            override fun onDataSelected(
                                                firstDate: Calendar?,
                                                secondDate: Calendar?,
                                                hours: Int,
                                                minutes: Int
                                            ) {
                                                firstDate?.let {
                                                    val date =
                                                        SimpleDateFormat("yyyy-MM-dd").format(it.time) + " $hours:$minutes"

                                                    val intent = Intent(Intent.ACTION_SENDTO).apply {
                                                        type = "*/*"
                                                        data =
                                                            Uri.parse("mailto:")
                                                        putExtra(Intent.EXTRA_EMAIL, arrayOf(event.organizer))
                                                        putExtra(Intent.EXTRA_SUBJECT, "${event.title} postpone")
                                                        putExtra(
                                                            Intent.EXTRA_TEXT,
                                                            "${event.title} will be postponed to $date"
                                                        )
                                                    }
                                                    startActivity(intent)

                                                    if (intent.resolveActivity(packageManager) != null) {
                                                    }

                                                } ?: this@MainActivity.makeToast("You should pick a day")
                                            }

                                            override fun onCancelled() {

                                            }
                                        })
                                        .show(supportFragmentManager, "TAG_SLYCALENDAR")
                                }
                            })
                            eventRecycler.layoutManager = LinearLayoutManager(this)
                        }, {
                            Timber.d(it)
                        })
                    }
                },
                {
                    Timber.d("No user Found")
                })
        )
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            Constants.READ_WRITE_CALENDER_REQUEST_CODE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    loadEvents()
                } else {
                    this.makeToast("You should accept permissions so that app could do its functionality ")
                }

            }
        }
    }


}
