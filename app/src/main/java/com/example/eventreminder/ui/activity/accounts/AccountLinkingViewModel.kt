package com.example.eventreminder.ui.activity.accounts

import androidx.lifecycle.ViewModel
import com.example.eventreminder.data.models.User
import com.example.eventreminder.data.repo.UserRepo

class AccountLinkingViewModel(private val userRepo: UserRepo) : ViewModel() {

    fun saveGoogleSignInState(state: Boolean) {
        this.userRepo.saveGoogleSignInState(state)
    }

    fun saveFacebookSignInState(state: Boolean) {
        this.userRepo.saveFacebookSignInState(state)
    }

    fun saveUserData(user: User){
        this.userRepo.saveUserDataInFile(user)
    }
}