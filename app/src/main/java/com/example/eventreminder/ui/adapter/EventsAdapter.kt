package com.example.eventreminder.ui.adapter

import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.RecyclerView
import com.example.eventreminder.R
import com.example.eventreminder.data.models.Event
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class EventsAdapter(val events: ArrayList<Event>, val onPostponeClickListner: OnPostponeClickListner) :
    RecyclerView.Adapter<EventsAdapter.EventViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder = EventViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.event_item_layout, parent, false)
    )

    override fun getItemCount(): Int = events.size

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        holder.eventTitle.text = events[position].title
        holder.eventDate.text = SimpleDateFormat("yyyy-MM-dd").format(Date(events[position].date))
        val hourFormatter: SimpleDateFormat = SimpleDateFormat("h:mm a")
        holder.eventTimes.text =
            "from ${hourFormatter.format(Date(events[position].date))} to ${hourFormatter.format(Date(events[position].endTime))}"
        hourFormatter.applyPattern("h:mm")
        holder.eventDuration.text =
            "Duration ${hourFormatter.format(Date(events[position].endTime - events[position].date))} Hours"

        events[position].location?.let {
            if (it.isNotEmpty())
                holder.eventLocation.text = it
        }
        holder.postponeBtn.setOnClickListener {
            onPostponeClickListner.onPostponeClick(events[position])
        }
        if (events[position].hasConflict) {
            holder.postponeLayout.visibility = View.VISIBLE
        }
    }

    interface OnPostponeClickListner {
        fun onPostponeClick(event: Event)
    }

    class EventViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val eventDate: TextView = itemView.findViewById(R.id.eventDate)
        val eventTitle: TextView = itemView.findViewById(R.id.eventTitle)
        val eventTimes: TextView = itemView.findViewById(R.id.eventTimes)
        val eventDuration: TextView = itemView.findViewById(R.id.eventDuration)
        val eventLocation: TextView = itemView.findViewById(R.id.eventLocation)
        val postponeBtn: Button = itemView.findViewById(R.id.postpone)
        val postponeLayout:LinearLayout = itemView.findViewById(R.id.postponeLayout)
    }
}