package com.example.eventreminder.ui.activity.accounts

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.eventreminder.data.repo.UserRepo

class AccountLinkingViewModelFactory(private val userRepo: UserRepo) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AccountLinkingViewModel(userRepo) as T
    }
}