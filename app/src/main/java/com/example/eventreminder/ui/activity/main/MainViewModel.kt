package com.example.eventreminder.ui.activity.main

import androidx.lifecycle.ViewModel
import com.example.eventreminder.data.models.Event
import com.example.eventreminder.data.models.User
import com.example.eventreminder.data.repo.EventsRepo
import com.example.eventreminder.data.repo.UserRepo
import io.reactivex.Maybe
import io.reactivex.Observable

class MainViewModel(private val userRepo: UserRepo, private val eventsRepo: EventsRepo) : ViewModel() {


    fun userData(): Maybe<User> {
        return userRepo.userData()
    }

    fun loadEvents(user: User): Observable<ArrayList<Event>>? {
        user.googleEmail?.let {
            return this.eventsRepo.GoogleCalendarEvents(it).map {
                val checkedEvents: ArrayList<Event> = ArrayList()
                for (toCheckEvent in it) {
                    for (event in it) {
                        if (event.id != toCheckEvent.id) {
                            toCheckEvent.isOverlapped(event)
                        }
                    }
                    checkedEvents.add(toCheckEvent)
                }

                checkedEvents
            }
        } ?: return null
    }

    fun checkEventConflicts(events: ArrayList<Event>): ArrayList<Event> {
        val checkedEvents: ArrayList<Event> = ArrayList()
        for (toCheckEvent in events) {
            for (event in events) {
                toCheckEvent.isOverlapped(event)
            }
            checkedEvents.add(toCheckEvent)
            events.remove(toCheckEvent)
        }

        return checkedEvents
    }
}