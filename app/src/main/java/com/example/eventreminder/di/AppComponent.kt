package com.example.eventreminder.di

import com.example.eventreminder.ui.activity.accounts.AccountsLinkingActivity
import com.example.eventreminder.ui.activity.details.DetailsActivity
import com.example.eventreminder.ui.activity.main.MainActivity
import dagger.Component

@Component(modules = [RepoModule::class, StorageModule::class, NetworkModule::class])
interface AppComponent {
    fun inject(mainActivity: MainActivity)
    fun inject(accountsLinkingActivity: AccountsLinkingActivity)
    fun inject(detailsActivity: DetailsActivity)
}