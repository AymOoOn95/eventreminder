package com.example.eventreminder.di

import android.content.ContentResolver
import com.example.eventreminder.data.network.OpenWeatherApi
import com.example.eventreminder.data.repo.EventsRepo
import com.example.eventreminder.data.repo.UserRepo
import com.example.eventreminder.data.repo.WeatherRepo
import com.example.eventreminder.data.storage.UserFileStorage
import dagger.Module
import dagger.Provides

@Module
class RepoModule(val contentResolver: ContentResolver) {

    @Provides
    fun provideGoogleEventRepo(): EventsRepo {
        return EventsRepo(contentResolver)
    }

    @Provides
    fun provideUserRepo(userFileStorage: UserFileStorage): UserRepo {
        return UserRepo(userFileStorage)
    }

    @Provides
    fun provideWeatherRepo(openWeatherApi: OpenWeatherApi): WeatherRepo {
        return WeatherRepo(openWeatherApi)
    }
}