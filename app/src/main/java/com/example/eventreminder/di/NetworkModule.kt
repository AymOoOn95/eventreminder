package com.example.eventreminder.di

import com.example.eventreminder.data.network.OpenWeatherApi
import com.example.eventreminder.util.Constants
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class NetworkModule {

    @Provides
    fun provideOpenWeatherApi(): OpenWeatherApi {
        return Retrofit.Builder().baseUrl(Constants.BASE_URL).addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create()).build().create(OpenWeatherApi::class.java)
    }
}