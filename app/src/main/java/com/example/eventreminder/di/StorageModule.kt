package com.example.eventreminder.di

import android.content.Context
import com.example.eventreminder.data.storage.SharedPrefUserFileStorage
import com.example.eventreminder.data.storage.UserFileStorage
import com.example.eventreminder.util.Constants
import dagger.Module
import dagger.Provides

@Module
class StorageModule(val context: Context) {

    @Provides
    fun provideUserFileStorage(): UserFileStorage {
        return SharedPrefUserFileStorage(context.getSharedPreferences(Constants.SHARED_FILE_NAME, Context.MODE_PRIVATE))
    }
}